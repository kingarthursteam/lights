--************************************************************************************
--* FUNCTIONS                                                                        *
--************************************************************************************

function lights.searchlight_on(pos)
	local light_node = { name="lights:light" }
	local range = 30

	-- West -X
	for i = 1, range do
		local pos = {x=pos.x-i,y=pos.y-i,z=pos.z}
		if minetest.get_node(pos).name == "air" then 
			minetest.set_node(pos, light_node )
		else
			break
		end
	end

	-- East +X
	for i = 1, range do
		local pos = {x=pos.x+i,y=pos.y-i,z=pos.z}
		if minetest.get_node(pos).name == "air" then 
			minetest.set_node(pos, light_node )
		else
			break
		end
	end

	-- real_north -Z
	for i = 1, range do
		local pos = {x=pos.x,y=pos.y-i,z=pos.z-i}
		if minetest.get_node(pos).name == "air" then 
			minetest.set_node(pos, light_node )
		else
			break
		end
	end

	-- North +Z
	for i = 1, range do
		local pos = {x=pos.x,y=pos.y-i,z=pos.z+i}
		if minetest.get_node(pos).name == "air" then 
			minetest.set_node(pos, light_node )
		else
			break
		end
	end

end

function lights.searchlight_off(pos)
	local lights_name = "lights:light"
	for i = 1, 30 do
		local west = {x=pos.x-i,y=pos.y-i,z=pos.z}
		local east = {x=pos.x+i,y=pos.y-i,z=pos.z}
		local south = {x=pos.x,y=pos.y-i,z=pos.z-i}
		local north = {x=pos.x,y=pos.y-i,z=pos.z+i}

		if minetest.get_node(west).name == lights_name then
			minetest.remove_node(west)
		end
		if minetest.get_node(east).name == lights_name then
			minetest.remove_node(east)
		end
		if minetest.get_node(south).name == lights_name then
			minetest.remove_node(south)
		end
		if minetest.get_node(north).name == lights_name then
			minetest.remove_node(north)
		end
	end
end

function lights.toggle_searchlight(pos, node)
	if node.name == "lights:searchlight" then
		minetest.swap_node(pos, {name="lights:searchlight_on"})
		lights.searchlight_on(pos)
	elseif node.name == "lights:searchlight_on" then
		minetest.swap_node(pos, {name="lights:searchlight"})
		lights.searchlight_off(pos)
	end
end

function lights.spotlight_off(pos)
	local lights_name = "lights:light"
	for i = 1, 19, 1 do
		local pos = {x=pos.x,y=pos.y+i,z=pos.z}
		if minetest.get_node(pos).name == lights_name then
			minetest.remove_node(pos)
		end
	end
end

function lights.spotlight_on(pos)
	local lights_node = {name="lights:light"}
	for i = 1, 19 do
		local pos = {x=pos.x,y=pos.y+i,z=pos.z}
		if minetest.get_node(pos).name == "air" then
			minetest.set_node(pos, lights_node)
		else
			break
		end
	end
end

function lights.toggle_spotlight(pos, node)
	if node.name == "lights:spotlight" then
		minetest.swap_node(pos, {name="lights:spotlight_on"})
		lights.spotlight_on(pos)
	elseif node.name == "lights:spotlight_on" then
		minetest.swap_node(pos, {name="lights:spotlight"})
		lights.spotlight_off(pos)
	end
end

function lights.daylight(pos,node)
	pos = {x=pos.x,y=pos.y-3,z=pos.z}-- go down 3 nodes.

	for i = 1, 2, 1 do -- place 2 nodes of lights:daylight
		local ontop = {x=pos.x,y=pos.y+i,z=pos.z}
		local node  = minetest.get_node_or_nil(ontop)

		if node ~= nil and node.name == "air" then
			minetest.set_node(ontop,{name='lights:daylight'})
		end
	end
	minetest.after(1, function(pos) -- wait a secound
		for i = 1, 2, 1 do -- remove both nodes.
			local ontop = {x=pos.x,y=pos.y+i,z=pos.z}
			local node  = minetest.get_node_or_nil(ontop)

			if node ~= nil and node.name == "lights:daylight" then
				minetest.remove_node(ontop)
			end
		end
	end, pos)
	-- the first node of air have now dayligt at day and nightlight in night.
	-- perfectly for underground farms
end
